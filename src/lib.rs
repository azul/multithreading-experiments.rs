use rand::prelude::*;
use std::env;
use std::{sync, thread, time};
use sequoia_store;
use sequoia_core;
use tempdir::TempDir;

pub fn run() {
    println!("Hello, world!");
    let args = env::args().skip(1);
    let mutex = sync::Arc::new(sync::Mutex::new(Vec::new()));

    for argument in args {
        let mutex = sync::Arc::clone(&mutex);
        let dir = TempDir::new("seq").unwrap();
        thread::spawn(move || {
            let x: u8 = random();
            let ctx = sequoia_core::Context::configure()
                .home(&dir.path())
                .network_policy(sequoia_core::NetworkPolicy::Offline)
                .build()
                .unwrap();
            let store = sequoia_store::Mapping::open(&ctx, "realm", "name").unwrap();
            let binding = store.lookup("email_address@example.example");
            assert!(binding.is_err());
            thread::sleep(time::Duration::from_millis(x.into()));
            let mut collection = mutex.lock().unwrap();
            collection.push(argument.to_string());
            println!("{} {}",x, argument);
        });
    }
    thread::sleep(time::Duration::from_millis(1000));
    println!("{:?}", mutex.lock().unwrap());
}
